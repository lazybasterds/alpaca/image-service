package main

import (
	"github.com/mongodb/mongo-go-driver/bson/primitive"
)

// Image contains information for a particular image
type Image struct {
	ID         primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Length     float64            `bson:"length" json:"length"`
	ChunkSize  float64            `bson:"chunkSize" json:"chunkSize"`
	UploadDate interface{}        `bson:"uploadDate" json:"uploadDate"`
	Filename   string             `bson:"filename" json:"filename"`
	Data       string             `json:"data"`
}
