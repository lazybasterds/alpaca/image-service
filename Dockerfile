FROM golang:1.12 as intermediate

# Add metada that this is an intermediate stage
LABEL stage="intermediate"

# Install go-dep
#RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

WORKDIR /go/src/gitlab.com/lazybasterds/alpaca/image-service

COPY . .

# Run the dep ensure update
#RUN dep ensure -vendor-only
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo


FROM golang:1.12

RUN mkdir /app
WORKDIR /app

COPY --from=intermediate /go/src/gitlab.com/lazybasterds/alpaca/image-service/image-service .

ENV MICRO_REGISTRY=mdns

CMD ["./image-service"]